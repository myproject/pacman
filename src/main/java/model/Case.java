package model;

import enumeration.StateCase;

public class Case {
    private final Vector position;
    private StateCase etat;

    public Case(Vector position, StateCase etat) {
        this.position = position;
        this.etat = etat;
    }

    public Vector getPosition() {
        return position;
    }

    public StateCase getEtat() {
        return etat;
    }

    public void setEtat(StateCase etat) {
        this.etat = etat;
    }
}
